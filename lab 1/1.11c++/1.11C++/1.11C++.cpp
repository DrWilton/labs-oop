// 1.11C++.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
	setlocale(LC_ALL, "Russian");
	cout << setiosflags(ios::left) << setw(10) << "Фамилия" << setw(10) << "Имя" << setw(15) << "Адрес" << setw(19) << "Город" << endl;
	cout << setiosflags(ios::left) << setw(10) << "Петров" << setw(10) << "Василий" << setw(15) << "Кленовая 16" << setw(19) << "Санкт-Петербург" << endl;
	cout << setiosflags(ios::left) << setw(10) << "Иванов" << setw(10) << "Сергей" << setw(15) << "Осиновая 3" << setw(19) << "Находка" << endl;
	cout << setiosflags(ios::left) << setw(10) << "Сидоров" << setw(10) << "Иван" << setw(15) << "Березовая 21" << setw(19) << "Калининград" << endl;
	return 0;
}


