// 1.12с++.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <math.h>
using namespace std;

int main()
// новый 1 фунт 100 пен.
// старый 1 фут 240 пен
// 1 шиллинг 12 пен
{
	double np, cpound,decpound,shilpen , shil, pen;
	cin >> np;
	cpound = floor(np);
	decpound = np - cpound;
	shilpen = (decpound * 240)/12;
	shil = floor(shilpen);
	pen = shilpen - shil;
	if (pen <= 0.9)
		pen *= 10;
	else
		pen *= 100;
	cout << cpound << '.' << shil << '.' << pen;
}

